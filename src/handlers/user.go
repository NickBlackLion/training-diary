package handlers

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"strings"
	"training_diary/src/models"
	"training_diary/src/utils"
)

func CreateUser(w http.ResponseWriter, r *http.Request) {
	var user models.User
	json.NewDecoder(r.Body).Decode(&user)
	data, status := user.Create()
	utils.Respond(w, data, status)
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {

}

func DeleteUser(w http.ResponseWriter, r *http.Request) {

}

func GetUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	user, err := models.GetUser(vars["id"])
	if err != nil {
		utils.Respond(w, utils.Message("User doesn't exist"), 404)
	} else {
		utils.Respond(w, user, 200)
	}
}

func GetUsers(w http.ResponseWriter, r *http.Request) {

}

func SignIn(w http.ResponseWriter, r *http.Request) {
	var user models.User
	json.NewDecoder(r.Body).Decode(&user)
	data := models.Login(user.Email, user.Password)
	utils.Respond(w, data, 200)
}

func SignOut(w http.ResponseWriter, r *http.Request) {
	token := strings.Split(r.Header.Get("Authorization"), " ")[1]
	data := models.Logout(token)
	utils.Respond(w, data, 200)
}
