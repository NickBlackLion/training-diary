package router

import (
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"log"
	"net/http"
	"training_diary/src/handlers"
)

var router = mux.NewRouter().StrictSlash(true)

func UserRouter() {
	router.HandleFunc("/api/user/sign-up", handlers.CreateUser).Methods("POST")
	router.HandleFunc("/api/user/sign-in", handlers.SignIn).Methods("POST")
	router.HandleFunc("/api/user/sign-out", handlers.SignOut).Methods("GET")
	router.HandleFunc("/api/user/{id:[a-zA-Z0-9]+}", handlers.GetUser).Methods("GET")
}

func RunServer() {
	router.Use(handlers.JwtAuthentication)
	c := cors.New(cors.Options{
		AllowedOrigins: []string{"http://localhost:3000"},
	})
	UserRouter()
	handler := c.Handler(router)
	log.Fatal(http.ListenAndServe(":10000", handler))
}
