package utils

import (
	"encoding/json"
	"log"
	"net/http"
)

func Fatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func Message(message string) map[string]interface{} {
	return map[string]interface{}{"message": message}
}

func Respond(w http.ResponseWriter, data interface{}, status int) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(data)
}
