package models

import (
	"github.com/dgrijalva/jwt-go"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"os"
	"strings"
	"time"
	"training_diary/src/mongo_utils"
	"training_diary/src/utils"
)

type User struct {
	Email     string    `bson:"email"`
	Password  string    `bson:"password"`
	FirstName string    `bson:"first_name"`
	LastName  string    `bson:"last_name"`
	Birthday  time.Time `bson:"birthday"`
	Token     string    `bson:"token"`
	CreatedAt time.Time `bson:"created_at"`
}

type ResponseUser struct {
	Email     string    `bson:"email"`
	FirstName string    `bson:"first_name"`
	LastName  string    `bson:"last_name"`
	Birthday  time.Time `bson:"birthday"`
	CreatedAt time.Time `bson:"created_at"`
}

type Token struct {
	UserEmail string
	Date      time.Time
	jwt.StandardClaims
}

func (user *User) Validate() (map[string]interface{}, bool) {
	if !strings.Contains(user.Email, "@") {
		return utils.Message("Email address is required"), false
	}

	if len(user.Password) < 6 {
		return utils.Message("Password is required"), false
	}

	//проверка на наличие ошибок и дубликатов электронных писем
	client, ctx, coll := mongo_utils.GetCollection("user")
	defer client.Disconnect(ctx)

	var temp User
	err := coll.FindOne(ctx, bson.D{{Key: "email", Value: user.Email}}).Decode(&temp)

	if err == nil {
		return utils.Message("Email address already in use by another user."), false
	}

	return utils.Message("Requirement passed"), true
}

func (user *User) Create() (map[string]interface{}, int) {
	if resp, ok := user.Validate(); !ok {
		return resp, http.StatusBadRequest
	}

	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	user.Password = string(hashedPassword)
	user.CreatedAt = time.Now().UTC()

	client, ctx, coll := mongo_utils.GetCollection("user")
	defer client.Disconnect(ctx)

	//Создать новый токен JWT для новой зарегистрированной учётной записи
	tk := &Token{UserEmail: user.Email, Date: time.Now()}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	tokenString, _ := token.SignedString([]byte(os.Getenv("token_password")))
	user.Token = tokenString

	coll.InsertOne(ctx, user)

	user.Password = "" //удалить пароль

	response := utils.Message("Account has been created")
	response["user"] = user
	return response, http.StatusCreated
}

func Login(email, password string) map[string]interface{} {
	client, ctx, coll := mongo_utils.GetCollection("user")
	defer client.Disconnect(ctx)

	user := &User{}

	findObj := bson.D{{Key: "email", Value: email}}
	err := coll.FindOne(ctx, findObj).Decode(&user)
	resp := mongo_utils.MongoError(err, "Email address not found")

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword { //Пароль не совпадает!!
		return utils.Message("Invalid login credentials. Please try again")
	}

	//Создать токен JWT
	tk := &Token{UserEmail: user.Email, Date: time.Now()}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	tokenString, _ := token.SignedString([]byte(os.Getenv("token_password")))

	update := bson.D{{"$set", bson.D{{"token", tokenString}}}}
	err = coll.FindOneAndUpdate(ctx, findObj, &update).Decode(&user)
	resp = mongo_utils.MongoError(err, "Failed token")

	if resp == nil {
		user.Password = ""
		user.Token = tokenString
		resp = utils.Message("Logged In")
		resp["user"] = user
	}

	return resp
}

func Logout(token string) map[string]interface{} {
	client, ctx, coll := mongo_utils.GetCollection("user")
	defer client.Disconnect(ctx)

	user := &ResponseUser{}

	update := bson.D{{"$set", bson.D{{"token", ""}}}}
	err := coll.FindOneAndUpdate(ctx, bson.D{{Key: "token", Value: token}}, &update).Decode(&user)
	resp := mongo_utils.MongoError(err, "User not found")

	if resp == nil {
		resp = utils.Message("Logged out")
		resp["user"] = user
	}

	return resp
}

func CheckToken(token string) (err error) {
	var user ResponseUser
	client, ctx, coll := mongo_utils.GetCollection("user")
	defer client.Disconnect(ctx)
	err = coll.FindOne(ctx, bson.D{{"token", token}}).Decode(&user)
	return err
}

func GetUser(id string) (user ResponseUser, err error) {
	client, ctx, coll := mongo_utils.GetCollection("user")
	defer client.Disconnect(ctx)
	objId, _ := primitive.ObjectIDFromHex(id)
	err = coll.FindOne(ctx, bson.D{{"_id", objId}}).Decode(&user)
	return user, err
}
