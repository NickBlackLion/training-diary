package models

import "time"

type Run struct {
	Date         time.Time `bson:"date"`
	StartTime    time.Time `bson:"start_time"`
	EndTime      time.Time `bson:"end_time"`
	AverageSpeed float32   `bson:"average_speed"`
	MaxSpeed     float32   `bson:"max_speed"`
	Distance     float32   `bson:"distance"`
	TrainingTime time.Time `bson:"training_time"`
	User         `bson:"user"`
}

type PushUps struct {
	Date   time.Time `bson:"date"`
	Amount []int     `bson:"amount"`
	User   `bson:"user"`
}

type SitUp struct {
	Date   time.Time `bson:"date"`
	Amount []int     `bson:"amount"`
	User   `bson:"user"`
}

type PullUps struct {
	Date   time.Time `bson:"date"`
	Amount []int     `bson:"amount"`
	User   `bson:"user"`
}
