package mongo_utils

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"time"
	"training_diary/src/utils"
)

var dbName = "training_diary"

func getConnection() (client *mongo.Client, ctx context.Context) {
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))
	utils.Fatal(err)

	ctx, _ = context.WithTimeout(context.Background(), 10*time.Second)

	err = client.Connect(ctx)
	utils.Fatal(err)

	err = client.Ping(ctx, readpref.Primary())
	utils.Fatal(err)

	return client, ctx
}

func GetCollection(collectionName string) (client *mongo.Client, context context.Context, collection *mongo.Collection) {
	client, ctx := getConnection()

	db := client.Database(dbName)
	collection = db.Collection(collectionName)

	return client, ctx, collection
}

func MongoError(err error, errorMessage string) map[string]interface{} {
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return utils.Message(errorMessage)
		}
		return utils.Message("Connection error. Please retry")
	}

	return nil
}
